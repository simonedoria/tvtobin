// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © simoNCode

//@version=4
strategy("simoNCode Trend Follower", initial_capital=2500, default_qty_type=strategy.percent_of_equity, commission_type=strategy.commission.percent, commission_value=0.06, slippage = 2, default_qty_value=60, overlay=false, calc_on_every_tick=true)

//definizione input LONG

start = timestamp(input(2018, "start year"), input(1, "start month"), input (1, "start day"), 00, 00)
average = input (title="Source MA Type", type=input.string, defval="EMA",options=["EMA","SMA"])
len = input(113, minval=1, title="Source MA Length")
slopeFlen = input (16,title="Fast Slope MA Length")
slopeSlen= input (28,title="Slow Slope MA Length") 

trendfilter=input(true,title="Trend Filter")
trendfilterperiod=input(160,title="Trend Filter MA Period")
trendfiltertype=input (title="Trend Filter MA Type", type=input.string, defval="SMA",options=["EMA","SMA"])

volatilityfilter=input(true,title="Volatility Filter")
volatilitystdevlength=input(30,title="Vol Filter STDev Length")
volatilitystdevmalength=input(37,title="Vol Filter STDev MA Length")

sl_inp = input(17, title='Stop Loss %', type=input.float)/100
tp_inp = input(5, title='Take Profit %', type=input.float)/100

//definizione input SHORT

averageShort = input (title="Source MA Type Short", type=input.string, defval="EMA",options=["EMA","SMA"])
lenShort = input(113, minval=1, title="Source MA Length Short")
slopeFlenShort = input (16,title="Fast Slope MA Length Short")
slopeSlenShort= input (28,title="Slow Slope MA Length Short") 

trendfilterShort=input(true,title="Trend Filter Short")
trendfilterperiodShort=input(160,title="Trend Filter MA Period Short")
trendfiltertypeShort=input (title="Trend Filter MA Type Short", type=input.string, defval="SMA",options=["EMA","SMA"])

volatilityfilterShort =input(true,title="Volatility Filter Short")
volatilitystdevlengthShort=input(36,title="Vol Filter STDev Length Short")
volatilitystdevmalengthShort=input(38,title="Vol Filter STDev MA Length Short")

sl_inpShort = input(2.0, title='Stop Loss % Short', type=input.float)/100
tp_inpShort = input(4.0, title='Take Profit % Short', type=input.float)/100
//variabili per long

out = if average == "EMA" 
    ema(hlc3,len)
else
    sma(hlc3,len)
    
slp = change(out)/out

emaslopeF = ema(slp,slopeFlen)
emaslopeS = ema(slp,slopeSlen)

//variabili per sort

outShort = if averageShort == "EMA" 
    ema(hlc3,lenShort)
else
    sma(hlc3,lenShort)
    
slpShort = change(outShort)/outShort

emaslopeFShort = ema(slpShort,slopeFlenShort)
emaslopeSShort = ema(slpShort,slopeSlenShort)


//variabili accessorie e condizioni

TrendConditionL=if trendfiltertype =="EMA"
    close>ema(close,trendfilterperiod)
else
    close>sma(close,trendfilterperiod)
    
TrendConditionS=if trendfiltertypeShort =="EMA"
    close<ema(close,trendfilterperiodShort)
else
    close<sma(close,trendfilterperiodShort)

VolatilityCondition=stdev(hlc3,volatilitystdevlength)>sma(stdev(hlc3,volatilitystdevlength),volatilitystdevmalength)
VolatilityConditionShort=stdev(hlc3,volatilitystdevlengthShort)>sma(stdev(hlc3,volatilitystdevlengthShort),volatilitystdevmalengthShort)

ConditionEntryL= if trendfilter == true
    if volatilityfilter == true
        emaslopeF>emaslopeS and TrendConditionL and VolatilityCondition
    else
        emaslopeF>emaslopeS and TrendConditionL
else
    if volatilityfilter == true
        emaslopeF>emaslopeS and VolatilityCondition
    else 
        emaslopeF>emaslopeS

ConditionEntryS= if trendfilterShort == true
    if volatilityfilterShort == true
        emaslopeFShort<emaslopeSShort and TrendConditionS and VolatilityConditionShort
    else 
        emaslopeFShort<emaslopeSShort and TrendConditionS
else
    if volatilityfilterShort == true
        emaslopeFShort<emaslopeSShort and VolatilityConditionShort
    else
        emaslopeFShort<emaslopeSShort

ConditionExitL=crossunder(emaslopeF,emaslopeS)

ConditionExitS=crossover(emaslopeFShort,emaslopeSShort)

// stop loss and take profit value
stop_level = strategy.position_avg_price * (1 - sl_inp)
take_level = strategy.position_avg_price * (1 + tp_inp)


//#####################################################################
alength=input(11, title="Wave A Length"), blength=input(25, title="Wave B Length"), clength=input(50, title="Wave C Length")
lengthMA=input(4, title="Wave SMA Length")
mse=input(false, title="Identify Spikes/Exhaustions")
cutoff = input(10, title="Cutoff") 
ebc=input(false, title="Color Bars on Spikes/Exhaustions")
src=hlc3
ma(s,l) => ema(s,l)
wa=sma(src-ma(src, alength),lengthMA) 
wb=sma(src-ma(src, blength),lengthMA) 
wc=sma(src-ma(src, clength),lengthMA) 
wcf=(wb != 0) ? (wc/wb > cutoff) : false
wbf=(wa != 0) ? (wb/wa > cutoff) : false
 
//#############################  SUPERTRAND  ##########################################
atrPeriod = input(10, "ATR Length") 
factor = input(3, "Factor")

[supertrend, direction] = supertrend(factor, atrPeriod)
down = direction < 0
up = direction > 0
//#############################fINE SUPERTRAND ##############################################
//################### CLOSE AFTER N BARS ############################
enterIndex = 0.0
enterIndex := enterIndex[1]

inPosition = not na(strategy.position_size) and strategy.position_size < 0
if inPosition and na(enterIndex)
    enterIndex := bar_index

CLOSE_AFTER_BARS = input(8, title="CLOSE SHORT AFTER n BARS")
closeAfterBarsCondition = false
if not na(enterIndex) and bar_index - enterIndex + 1 >= CLOSE_AFTER_BARS and not ConditionEntryS
    closeAfterBarsCondition := true
    enterIndex := na
//ingressi e uscite

strategy.exit("ExitSLPShort", loss=stop_level, profit=take_level, when=closeAfterBarsCondition or (time > start and strategy.position_size < 0 and supertrend > 0 and ConditionExitS and (wa[0] < 0 and wb[0] < 0 and wc[0] < 0) and (wa[0] > wa[1] and wb[0] > wb[1] and wc[0] > wc[1])))
            
strategy.close("ExitSLPLong", when=time > start and strategy.position_size > 0 and supertrend < 0 and ( ConditionExitL and (wa[0] > 0 and wb[0] > 0 and wc[0] > 0) and (wa[0] < wa[1] and wb[0] < wb[1] and wc[0] < wc[1])) )

strategy.entry("SLPLong", long = true, when=time > start and (wa[0] > wa[1] and wb[0] > wb[1] and wc[0] > wc[1]) and ConditionEntryL)
        
strategy.entry("SLPShort", long = false, when=not closeAfterBarsCondition and time > start and (wa[0] < wa[1] and wb[0] < wb[1] and wc[0] < wc[1]) and ConditionEntryS)