const express = require('express')
const cors = require('cors')
const moment = require('moment-timezone');
const app = express.Router();
const core = require('./core.js');
const path = require("path");

moment.locale('it');

app.use(cors())
app.use(express.json({ limit: '100mb' }));
app.use(express.urlencoded({ limit: '100mb', extended: true, type: "application/json" }));
app.use(express.static(path.join(__dirname, '/static')));


app.post('/crypt', core.crypt);

app.post('/send', core.send);

app.post('/analyze', core.analyze);

module.exports = app;