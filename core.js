const encrypter = require('./encrypter');
//const Binance = require('binance-api-node').default;
require('dotenv').config();
var ccxt = require('ccxt');
const puppeteer = require('puppeteer');
const { Telegraf } = require('telegraf');
const bot = new Telegraf('1920314284:AAHDMFguc75m5kJ8wQlFsTtqEcDChEhAtFc');
const Decimal = require('decimal.js');
bot.launch();
/*
TODO:
formula per calcolare la percentuale
formula = balance * perc * leverage / BTC Price (Best Bid or Best Ask)
prendere il nationalCap dalla chiamata futuresLeverageBracket
prendere il valore minore fra formula e nationalcap / BTC Price
*/

var currentPosition = null;
var val = null;

module.exports = {
  async sendNew(req, res) {
    try {
      let message = req.body.message;
      message = encrypter.decryptStringWithRsaPrivateKey(message, 'rsa');
      let trade = JSON.parse(message);
      /*let obj = {
        telegram_chat_id: 12345,
        pair: 'BTC/USDT',
        side: 'SELL', // SELL
        type: 'MARKET', // LIMIT
        leverage: 10,
        takeProfit: 0, // value in pips
        stopLoss: 0, // value in pips
        isPercentage: true, // 0
        value: 5, // if percentange, else real value
        limitPrice: 11111, // if isPercentage = 0 --OPTIONAL
      }*/
      let obj = trade;
      // todo: prendo apiKey e secret tramite id chat
      let apiKey = process.env.API_KEY;
      let apisecret = process.env.API_SECRET;
      const client = new ccxt.ftx({
        apiKey: apiKey,
        secret: apisecret,
        enableRateLimit: true,
        options: {
          defaultType: 'future'
        }
      });
      //trade = obj;
      // imposto la leva scelta
      await client.load_markets();
      let mkt = client.markets[trade.pair];
      await client.private_post_account_leverage({
        symbol: mkt['id'],
        leverage: trade.leverage,
      })
      //await client.set_leverage(20);
      // prendo il current balance
      // prendo il pair price
      // eseguo il trade
      // calcolo percentuale
      let value = 0;
      let canPlaceClose = true;
      let price = 0;
      if (trade.type != 'CLOSE') {
        if (trade.isPercentage) {
          // let market = await client.fetchTicker(trade.pair);
          // let priceWhenOrder = market.info.last;
          // let balances = await client.fetchBalance();
          // let avBalance = balances.info.result[2].free;
          // let percentage = trade.value;
          // value = (avBalance * (percentage / 100) * trade.leverage) / priceWhenOrder;
          
          
          let to_use = await  client.fetchBalance();
          to_use = to_use.USDT.free;
          //to_use = Math.round((to_use + Number.EPSILON) * 1000) / 1000;
          price = await client.fetchTicker(trade.pair);
          price = price.last;
          //price = Math.round((price + Number.EPSILON) * 1000) / 1000;
          let decide_position_to_use = to_use / price;
          value = decide_position_to_use;
          //value = (avBalance * (percentage / 100));
        } else {
          value = trade.value;
        }
        //value = Math.round((value + Number.EPSILON) * 1000) / 1000;
      } else {
        // istruzioni per chiudere
        let positions = await client.fetchBalance();
        positions = positions.info.positions;
        symbolPosition = positions.filter(p => p.symbol = mkt['id']);
        symbolPosition = symbolPosition.filter(p => parseFloat(p['positionAmt']) != 0);
        if (symbolPosition.length > 0) {
          value = symbolPosition[0].positionAmt;
        } else {
          canPlaceClose = false;
        }
      }
      let order = null;
      value = value < mkt.limits.amount.min ? mkt.limits.amount.min: value;
      switch (trade.type) {
        case 'MARKET':
        // check se esisono posizioni aperte in caso le chiude
        let positions = await client.fetch_positions();
        let pos = positions.filter(p => p.future = mkt['id'] && p.entryPrice != null);
        if (pos.length > 0) {
          let valueToClose = pos[0].size;
          let side = valueToClose < 0 ? 'buy' : 'sell';
          order = await client.createOrder(mkt['symbol'], 'market', side, amount = Math.abs(valueToClose), { reduceOnly: true });
          bot.telegram.sendMessage(102073553, `Chiusura posizione di tipo ${side} su ${mkt['symbol']}`);
        }
        try {
          // apro la nuova posizione
          order = await client.createOrder(mkt['symbol'], 'market', obj.side, value);
          bot.telegram.sendMessage(102073553, `Nuovo ordine a mercato di tipo ${obj.side} su ${mkt['symbol']}\nPrezzo d'entrata: ${price}`);
        } catch (ex) {
          bot.telegram.sendMessage(102073553, `ERRORE: nel piazzare l'ordine ${obj.side} su ${mkt['symbol']}\n${ex}`);
          res.json({ success: "false", "message": `ERRORE: nel piazzare l'ordine ${obj.side} su ${mkt['symbol']} ${ex}` });
          return;
        }
        
        
        // try {
        //   let inverted_side = (obj.side == 'buy') ? 'sell' : 'buy';
        //   takeProfitPrice = price - ( ( price / 100 ) * 0.07 );
        //   let takeProfitParams = {'triggerPrice': takeProfitPrice, 'stopPrice': takeProfitPrice, 'reduceOnly': false, price: null };
        //   let takeorder = await client.createOrder(mkt['symbol'], 'takeProfit', inverted_side , value, takeProfitPrice, takeProfitParams);
        //   bot.telegram.sendMessage(102073553, `Nuovo ordine take profit ${inverted_side} su ${mkt['symbol']}\nPrezzo trigger: ${takeProfitPrice}`);
        // } catch (ex) {
        //   bot.telegram.sendMessage(102073553, `ERRORE: nel piazzare l'ordine take profit ${obj.side} su ${mkt['symbol']}\n${ex}`);
        //   res.json({ success: "false", "message": `ERRORE: nel piazzare l'ordine take profit ${obj.side} su ${mkt['symbol']} ${ex}` });
        //   return; 
        // } 
        break;
        case 'LIMIT':
        
        break;
        case 'CLOSE':
        
        break;
        
      }
      // piazzo eventuali ordini TP
      // piazzo eventuali ordini SL
      // salva su db il trade
      
      res.json({ success: "true" });
    } catch (ex) {
      console.log(ex);
      res.json(ex);
    } finally {
    }
  },
  async getPercentageValue(req, res) {
    let message = req.body.message;
    message = encrypter.decryptStringWithRsaPrivateKey(message, 'rsa');
    let trade = JSON.parse(message);
    let apiKey = 'A49PsZYkfx6D5ny8cdHgZsuDeK0e4kcA6QRCHQVT';//process.env.API_KEY;
    let apisecret = '6rJ0L_Pe25M3sxieySymus3t056Xi-qjTkQJ6wp6';//process.env.API_SECRET;
    const client = new ccxt.ftx({
      apiKey: apiKey,
      secret: apisecret,
      enableRateLimit: true,
      options: {
        defaultType: 'future'
      }
    });
    let to_use = await  client.fetchBalance();
    to_use = to_use.USDT.free;
    to_use = Math.round((to_use + Number.EPSILON) * 1000) / 1000;
    let price = await client.fetchTicker(trade.pair);
    price = price.last;
    price = Math.round((price + Number.EPSILON) * 1000) / 1000;
    let decide_position_to_use = to_use / price;
    let value = decide_position_to_use;
    value = (trade.value * (value / 100)) * trade.leverage;
    value = Math.round((value + Number.EPSILON) * 1000) / 1000;
    res.json({amount: value});
  },
  
  async send(req, res) {
    try {
      let message = req.body.message;
      message = encrypter.decryptStringWithRsaPrivateKey(message, 'rsa');
      let trade = JSON.parse(message);
      /*let obj = {
        telegram_chat_id: 12345,
        pair: 'BTC/USDT',
        side: 'SELL', // SELL
        type: 'MARKET', // LIMIT
        leverage: 10,
        takeProfit: 0, // value in pips
        stopLoss: 0, // value in pips
        isPercentage: true, // 0
        value: 5, // if percentange, else real value
        limitPrice: 11111, // if isPercentage = 0 --OPTIONAL
      }*/
      let obj = trade;
      // todo: prendo apiKey e secret tramite id chat
      let apiKey = 'A49PsZYkfx6D5ny8cdHgZsuDeK0e4kcA6QRCHQVT';//process.env.API_KEY;
      let apisecret = '6rJ0L_Pe25M3sxieySymus3t056Xi-qjTkQJ6wp6';//process.env.API_SECRET;
      const client = new ccxt.ftx({
        apiKey: apiKey,
        secret: apisecret,
        enableRateLimit: true,
        options: {
          defaultType: 'future'
        }
      });
      //trade = obj;
      // imposto la leva scelta
      await client.load_markets();
      let mkt = client.markets[trade.pair];
      await client.private_post_account_leverage({
        symbol: mkt['id'],
        leverage: trade.leverage,
      })
      // prendo il current balance
      // prendo il pair price
      // eseguo il trade
      // calcolo percentuale
      let value = 0;
      if (trade.type != 'CLOSE') {
        if (trade.isPercentage) {
          let to_use = await  client.fetchBalance();
          to_use = to_use.USDT.free;
          to_use = Math.round((to_use + Number.EPSILON) * 1000) / 1000;
          let price = await client.fetchTicker(trade.pair);
          price = price.last;
          price = Math.round((price + Number.EPSILON) * 1000) / 1000;
          let decide_position_to_use = to_use / price;
          let value = decide_position_to_use;
          value = Math.round((value + Number.EPSILON) * 1000) / 1000;
          let toMult = trade.value/100;
          value = new Decimal(value).times(toMult);
          value = value.times(trade.leverage);
          value = parseFloat(value.toPrecision());
          val = value;
          console.log(value);
        } else {
          value = trade.value;
          val = value;
        }
        //value = Math.round((value + Number.EPSILON) * 1000) / 1000;
        //console.log(value);
      }
      let order = null;
      switch (trade.type) {
        case 'MARKET':
        try {
          if (currentPosition != null && currentPosition != obj.side) {
            // check se esisono posizioni aperte in caso le chiude
            let positions = await client.fetch_positions();
            let pos = positions.filter(p => p.info.future = obj.pair && p.info.entryPrice != null);
            if (pos.length > 0) {
              let valueToClose = pos[0].info.size;
              order = await client.createOrder(mkt['symbol'], 'market', obj.side, amount = Math.abs(valueToClose), {reduceOnly: true});
              bot.telegram.sendMessage(102073553, `Chiusura posizione di tipo ${currentPosition} su ${mkt['symbol']}`);
            }
          }
          
          // apro la nuova posizione
          order = await client.createOrder(obj.pair, 'market', obj.side, val, {reduceOnly: false});
          bot.telegram.sendMessage(102073553, `Nuovo ordine a mercato di tipo ${obj.side} su ${obj.pair}`);
          currentPosition = obj.side;
        } catch (ex) {
          bot.telegram.sendMessage(102073553, `ERRORE: nel piazzare l'ordine ${obj.side} su ${obj.pair}\n${ex}`);
        }
        break;
        case 'LIMIT':
        order = await client.createOrder(mkt['symbol'], 'limit', obj.side, value, obj.limitPrice);
        bot.telegram.sendMessage(102073553, `Nuovo ordine LIMIT di tipo ${obj.side} su ${mkt['symbol']}`);
        break;
        case 'CLOSE':
        // check se esisono posizioni aperte in caso le chiude
        let sideToClose = obj.side == 'sell' ? 'buy': 'sell';
        let positions = await client.fetch_positions();
        let pos = positions.filter(p => p.info.future = obj.pair && p.info.entryPrice != null);
        if (pos.length > 0) {
          let valueToClose = pos[0].info.size;
          order = await client.createOrder(mkt['symbol'], 'market', sideToClose, amount = Math.abs(valueToClose), {reduceOnly: true});
          bot.telegram.sendMessage(102073553, `Chiusura posizione di tipo ${obj.side} su ${mkt['symbol']}`);
          currentPosition = null;
        }
        break;
        
      }
      // piazzo eventuali ordini TP
      // piazzo eventuali ordini SL
      // salva su db il trade
      
      res.json(order);
    } catch (ex) {
      console.log(ex);
      res.json(ex);
    } finally {
    }
  },
  async crypt(req, res) {
    let obj = req.body;
    /*let obj = {
      telegram_chat_id: 12345,
      pair: 'BTC/USDT',
      side: 'BUY', // SELL
      type: 'MARKET', // LIMIT
      leverage: 10,
      takeProfit: 0, // value in pips
      stopLoss: 0, // value in pips
      isPercentage: true, // 0
      value: 5, // if percentange, else real value
      limitPrice: 11111, // if isPercentage = 0 --OPTIONAL
    }*/
    
    //message = Buffer.from(JSON.stringify(obj)).toString('base64');
    message = encrypter.encryptStringWithRsaPublicKey(JSON.stringify(obj), 'rsa.pub');
    res.json({ encrypted: message });
  },
  async analyze(req, res) {
    let pair = req.body.pair;
    /*const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox']
    });*/
    const browser = await puppeteer.launch({
      headless: false,
      args: [
        '--start-maximized',
        '--no-sandbox'
      ]
    });
    const page = await browser.newPage();
    await page.setViewport({ width: 1920, height: 900 });
    await page.goto('https://it.tradingview.com', { waitUntil: 'networkidle2' });
    // LOGIN
    let btnMenuLogin = 'body > div.tv-main > div.tv-header.tv-header__top.js-site-header-container.tv-header--sticky.tv-header--promo.tv-header--animated > div.tv-header__inner > div.tv-header__area.tv-header__area--user > button.tv-header__user-menu-button.tv-header__user-menu-button--anonymous.js-header-user-menu-button';
    await page.waitForSelector(btnMenuLogin);
    await page.click(btnMenuLogin);
    let btnLogin = '.item-2IihgTnv';
    await page.waitForSelector(btnLogin);
    await page.click(btnLogin);
    let btnEmail = '.tv-signin-dialog__toggle-email';
    await page.waitForSelector(btnEmail);
    await page.click(btnEmail);
    let emailUserSelector = 'input[name=username]';
    let passUserSelector = 'input[name=password]';
    await page.waitForSelector(emailUserSelector);
    await page.waitForSelector(passUserSelector);
    await page.$eval(emailUserSelector, (el, value) => el.value = value, 'simonedoriasd@gmail.com');
    await page.$eval(passUserSelector, (el, value) => el.value = value, 'Anna.1992');
    let btnAccedi = '[id^=email-signin__submit-button__]';
    await page.waitForSelector(btnAccedi);
    await page.click(btnAccedi);
    await page.waitForTimeout(2000);
    // CHART PAGE
    await page.goto(`https://it.tradingview.com/chart/WuM2FB8U/?symbol=BINANCE%3A${pair}`);
    await page.waitForSelector('[class^="tab-"]');
    
    //hour settings
    let timeframeSelector = 'body > div.js-rootresizer__contents > div.layout__area--top > div > div > div:nth-child(1) > div.wrapOverflow-3obNZqvj > div > div > div > div > div:nth-child(2)';
    await page.waitForSelector(timeframeSelector);
    await page.click(timeframeSelector);
    //let h1Selector = '#overlap-manager-root > div > span > div.menuWrap-g78rwseC > div > div > div > div:nth-child(16)';
    let m15Selector = '#overlap-manager-root > div > span > div.menuWrap-g78rwseC > div > div > div > div:nth-child(11)';
    await page.waitForSelector(m15Selector);
    await page.click(m15Selector);
    // pattern
    let patternSelector = 'body > div.js-rootresizer__contents > div.layout__area--top > div > div > div:nth-child(1) > div.wrapOverflow-3obNZqvj > div > div > div > div > div:nth-child(3)';
    await page.waitForSelector(patternSelector);
    await page.click(patternSelector);
    let candeleSelector = '#overlap-manager-root > div > span > div.menuWrap-g78rwseC > div > div > div.item-2IihgTnv.withIcon-2IihgTnv.isActive-2IihgTnv';
    await page.click(candeleSelector);
    // cookie
    try {
      let cookieSelector = 'body > div:nth-child(8) > div > div > div > article > div.main-content-mxEBwIhg > div > button';
      page.waitForSelector(cookieSelector);
      await page.click(cookieSelector);
    } catch (exc) { 
      console.error(exc);
    }
    // tab strategy closed
    try {
      let tabStrategyClosed= '#footer-chart-panel > div.tabbar-37voAVwR > div:nth-child(1) > div:nth-child(4)';
      page.waitForSelector(tabStrategyClosed, {timeout: 1000});
      await page.click(tabStrategyClosed);
      await page.click(tabStrategyClosed);
    } catch (exc) {
      console.error(exc);
    }
    // tab strategy open
    try {
      let tabStrategyOpen = '#footer-chart-panel > div.tabbar-37voAVwR > div:nth-child(1) > div:nth-child(4)';
      page.waitForSelector(tabStrategyOpen, {timeout: 1000});
      await page.click(tabStrategyOpen);
    } catch (exc) { 
      console.error(exc);
    }
    // opening dialog
    let openInput = '.js-backtesting-open-format-dialog';
    await page.waitForSelector(openInput);
    await page.click(openInput);
    
    let obj = {
      sourceMAType: ['EMA', 'SMA'],
      sourceMALength: [100, 150],
      fastSlopeMaLen: [5, 15],
      slowSlopeMaLen: [16, 30],
      ultraSlowSlopeMaLen: [150, 250],
      trendFilterMaPeriod: [150, 250],
      trendFiltreMaType: ['EMA', 'SMA']
    }
    let betterByPerc = {};
    let betterPerc = 0;
    let betterByDrawdown = {};
    let betterDrawdown = 100;
    let sourceMaLengthSelector = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(6) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
    let fastSlopeMaLenSelector = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(6) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
    let slowSlopeMaLenSelector = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(8) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
    let ultraSlowSlopeMaLenSelector = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(10) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
    let trendFilterMaPeriod = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(13) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
    await page.waitForSelector(sourceMaLengthSelector);
    let results = [];
    for (let a = obj.sourceMALength[0]; a <= obj.sourceMALength[1]; a++) {
      //await page.$eval(sourceMaLengthSelector, (el, value) => el.value = value, a);
      await page.click(sourceMaLengthSelector);
      for (let i = 0; i < 4; i++) {
        await page.keyboard.press('Backspace');
      }
      await page.keyboard.type(a.toString());
      await page.waitForTimeout(500);
      await page.keyboard.press(String.fromCharCode(13));
      await page.waitForTimeout(500);
      for (let b = obj.fastSlopeMaLen[0]; b <= obj.fastSlopeMaLen[1]; b++) {
        //await page.$eval(fastSlopeMaLenSelector, (el, value) => el.value = value, b);
        await page.click(fastSlopeMaLenSelector);
        for (let i = 0; i < 4; i++) {
          await page.keyboard.press('Backspace');
        }
        await page.keyboard.type(b.toString());
        await page.keyboard.press(String.fromCharCode(13));
        for (let c = obj.slowSlopeMaLen[0]; c <= obj.slowSlopeMaLen[1]; c++) {
          //await page.$eval(slowSlopeMaLenSelector, (el, value) => el.value = value, c);
          await page.click(slowSlopeMaLenSelector);
          for (let i = 0; i < 4; i++) {
            await page.keyboard.press('Backspace');
          }
          await page.keyboard.type(c.toString());
          await page.keyboard.press(String.fromCharCode(13));
          /*for (let d = obj.ultraSlowSlopeMaLen[0]; d <= obj.ultraSlowSlopeMaLen[1]; d++) {
            //await page.$eval(ultraSlowSlopeMaLenSelector, (el, value) => el.value = value, d);
            await page.click(ultraSlowSlopeMaLenSelector);
            for (let i = 0; i < 4; i++) {
              await page.keyboard.press('Backspace');
            }
            await page.keyboard.type(d.toString());
            await page.keyboard.press(String.fromCharCode(13));*/
            for (let e = obj.trendFilterMaPeriod[0]; e <= obj.trendFilterMaPeriod[1]; e++) {
              //await page.$eval(trendFilterMaPeriod, (el, value) => el.value = value, e);
              await page.click(trendFilterMaPeriod);
              for (let i = 0; i < 4; i++) {
                await page.keyboard.press('Backspace');
              }
              await page.keyboard.type(e.toString());
              await page.keyboard.press(String.fromCharCode(13));
              await page.waitForTimeout(500);
              let percValSelector = 'div > div.report-data > div:nth-child(1) > p > span';
              await page.waitForSelector(percValSelector);
              let percValElement = await page.$(percValSelector);
              let drawdownSelector = 'div > div.report-data > div:nth-child(5) > p > span > span';
              let drawdownValElement = await page.$(drawdownSelector);
              await page.waitForSelector(drawdownSelector);
              let percValValue = await page.evaluate(el => el.textContent, percValElement);
              let drawdownValue = await page.evaluate(el => el.textContent, drawdownValElement);
              let o = {
                sourceMALength: a,
                fastSlopeMaLen: b,
                slowSlopeMaLen: c,
                ultraSlowSlopeMaLen: 'whatever',
                trendFilterMaPeriod: e,
                percValue: percValValue,
                drawdown: drawdownValue
              }
              results.push(o);
              perc = o.percValue.replace(' %', '');
              perc = parseFloat(perc);
              if (perc > betterPerc) {
                betterPerc = perc;
                betterByPerc = o;
                console.log('BETTER BY PERCENTAGE', o);
              }
              drawdownNum = o.drawdown.replace(' %', '');
              drawdownNum = parseFloat(drawdownNum);
              if (drawdownNum < betterDrawdown) {
                betterDrawdown = drawdownNum;
                betterByDrawdown = o;
                console.log('BETTER BY DRAWDOWN', o);
              }
            }
            //}
          }
        }
      }
      // csv creator
      const csv = new ObjectsToCsv(results);
      // Save to file:
      await csv.toDisk('./results.csv');
      res.json(better);
    },
    async analyzeTest(pair) {
      /*const browser = await puppeteer.launch({
        headless: true,
        args: ['--no-sandbox']
      });*/
      const browser = await puppeteer.launch({
        headless: true,
        args: [
          '--start-maximized',
          '--no-sandbox'
        ]
      });
      const page = await browser.newPage();
      await page.setViewport({ width: 1920, height: 900 });
      await page.goto('https://it.tradingview.com', { waitUntil: 'networkidle2' });
      // LOGIN
      let btnMenuLogin = 'body > div.tv-main > div.tv-header.tv-header__top.js-site-header-container.tv-header--sticky.tv-header--promo.tv-header--animated > div.tv-header__inner > div.tv-header__area.tv-header__area--user > button.tv-header__user-menu-button.tv-header__user-menu-button--anonymous.js-header-user-menu-button';
      await page.waitForSelector(btnMenuLogin);
      await page.click(btnMenuLogin);
      let btnLogin = '.item-2IihgTnv';
      await page.waitForSelector(btnLogin);
      await page.click(btnLogin);
      let btnEmail = '.tv-signin-dialog__toggle-email';
      await page.waitForSelector(btnEmail);
      await page.click(btnEmail);
      let emailUserSelector = 'input[name=username]';
      let passUserSelector = 'input[name=password]';
      await page.waitForSelector(emailUserSelector);
      await page.waitForSelector(passUserSelector);
      await page.$eval(emailUserSelector, (el, value) => el.value = value, 'simonedoriasd@gmail.com');
      await page.$eval(passUserSelector, (el, value) => el.value = value, 'Anna.1992');
      let btnAccedi = '[id^=email-signin__submit-button__]';
      await page.waitForSelector(btnAccedi);
      await page.click(btnAccedi);
      await page.waitForTimeout(2000);
      // CHART PAGE
      await page.goto(`https://it.tradingview.com/chart/WuM2FB8U/?symbol=BINANCE%3A${pair}`);
      await page.waitForSelector('[class^="tab-"]');
      
      //hour settings
      let timeframeSelector = 'body > div.js-rootresizer__contents > div.layout__area--top > div > div > div:nth-child(1) > div.wrapOverflow-3obNZqvj > div > div > div > div > div:nth-child(2)';
      await page.waitForSelector(timeframeSelector);
      await page.click(timeframeSelector);
      let h1Selector = '#overlap-manager-root > div > span > div.menuWrap-g78rwseC > div > div > div > div:nth-child(16)';
      await page.waitForSelector(h1Selector);
      await page.click(h1Selector);
      // pattern
      let patternSelector = 'body > div.js-rootresizer__contents > div.layout__area--top > div > div > div:nth-child(1) > div.wrapOverflow-3obNZqvj > div > div > div > div > div:nth-child(3)';
      await page.waitForSelector(patternSelector);
      await page.click(patternSelector);
      let candeleSelector = '#overlap-manager-root > div > span > div.menuWrap-g78rwseC > div > div > div.item-2IihgTnv.withIcon-2IihgTnv.isActive-2IihgTnv';
      await page.click(candeleSelector);
      // cookie
      try {
        let cookieSelector = 'body > div:nth-child(8) > div > div > div > article > div.main-content-mxEBwIhg > div > button';
        page.waitForSelector(cookieSelector);
        await page.click(cookieSelector);
      } catch (exc) { 
        console.error(exc);
      }
      // tab strategy closed
      try {
        let tabStrategyClosed= '#footer-chart-panel > div.tabbar-37voAVwR > div:nth-child(1) > div:nth-child(4)';
        page.waitForSelector(tabStrategyClosed, {timeout: 1000});
        await page.click(tabStrategyClosed);
        await page.click(tabStrategyClosed);
      } catch (exc) {
        console.error(exc);
      }
      // tab strategy open
      try {
        let tabStrategyOpen = '#footer-chart-panel > div.tabbar-37voAVwR > div:nth-child(1) > div:nth-child(4)';
        page.waitForSelector(tabStrategyOpen, {timeout: 1000});
        await page.click(tabStrategyOpen);
      } catch (exc) { 
        console.error(exc);
      }
      // opening dialog
      let openInput = '.js-backtesting-open-format-dialog';
      await page.waitForSelector(openInput);
      await page.click(openInput);
      
      let obj = {
        sourceMAType: ['EMA', 'SMA'],
        sourceMALength: [100, 150],
        fastSlopeMaLen: [5, 15],
        slowSlopeMaLen: [16, 30],
        ultraSlowSlopeMaLen: [150, 250],
        trendFilterMaPeriod: [150, 250],
        trendFiltreMaType: ['EMA', 'SMA']
      }
      let betterByPerc = {};
      let betterPerc = 0;
      let betterByDrawdown = {};
      let betterDrawdown = 100;
      let sourceMaLengthSelector = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(6) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
      let fastSlopeMaLenSelector = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(6) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
      let slowSlopeMaLenSelector = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(8) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
      let ultraSlowSlopeMaLenSelector = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(10) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
      let trendFilterMaPeriod = '#overlap-manager-root > div > div > div.dialog-2AogBbC7.dialog-2cMrvu9r.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp > div > div.scrollable-2CTvqFKf > div > div:nth-child(13) > div > span > span.inner-slot-2OKMGqSc.inner-middle-slot-FxLdcHA0 > input';
      await page.waitForSelector(sourceMaLengthSelector);
      let results = [];
      for (let a = obj.sourceMALength[0]; a <= obj.sourceMALength[1]; a++) {
        //await page.$eval(sourceMaLengthSelector, (el, value) => el.value = value, a);
        await page.click(sourceMaLengthSelector);
        for (let i = 0; i < 4; i++) {
          await page.keyboard.press('Backspace');
        }
        await page.keyboard.type(a.toString());
        await page.waitForTimeout(500);
        await page.keyboard.press(String.fromCharCode(13));
        await page.waitForTimeout(500);
        for (let b = obj.fastSlopeMaLen[0]; b <= obj.fastSlopeMaLen[1]; b++) {
          //await page.$eval(fastSlopeMaLenSelector, (el, value) => el.value = value, b);
          await page.click(fastSlopeMaLenSelector);
          for (let i = 0; i < 4; i++) {
            await page.keyboard.press('Backspace');
          }
          await page.keyboard.type(b.toString());
          await page.keyboard.press(String.fromCharCode(13));
          for (let c = obj.slowSlopeMaLen[0]; c <= obj.slowSlopeMaLen[1]; c++) {
            //await page.$eval(slowSlopeMaLenSelector, (el, value) => el.value = value, c);
            await page.click(slowSlopeMaLenSelector);
            for (let i = 0; i < 4; i++) {
              await page.keyboard.press('Backspace');
            }
            await page.keyboard.type(c.toString());
            await page.keyboard.press(String.fromCharCode(13));
            for (let d = obj.ultraSlowSlopeMaLen[0]; d <= obj.ultraSlowSlopeMaLen[1]; d++) {
              //await page.$eval(ultraSlowSlopeMaLenSelector, (el, value) => el.value = value, d);
              await page.click(ultraSlowSlopeMaLenSelector);
              for (let i = 0; i < 4; i++) {
                await page.keyboard.press('Backspace');
              }
              await page.keyboard.type(d.toString());
              await page.keyboard.press(String.fromCharCode(13));
              for (let e = obj.trendFilterMaPeriod[0]; e <= obj.trendFilterMaPeriod[1]; e++) {
                //await page.$eval(trendFilterMaPeriod, (el, value) => el.value = value, e);
                await page.click(trendFilterMaPeriod);
                for (let i = 0; i < 4; i++) {
                  await page.keyboard.press('Backspace');
                }
                await page.keyboard.type(e.toString());
                await page.keyboard.press(String.fromCharCode(13));
                await page.waitForTimeout(500);
                let percValSelector = 'div > div.report-data > div:nth-child(1) > p > span';
                await page.waitForSelector(percValSelector);
                let percValElement = await page.$(percValSelector);
                let drawdownSelector = 'div > div.report-data > div:nth-child(5) > p > span > span';
                let drawdownValElement = await page.$(drawdownSelector);
                await page.waitForSelector(drawdownSelector);
                let percValValue = await page.evaluate(el => el.textContent, percValElement);
                let drawdownValue = await page.evaluate(el => el.textContent, drawdownValElement);
                let o = {
                  sourceMALength: a,
                  fastSlopeMaLen: b,
                  slowSlopeMaLen: c,
                  ultraSlowSlopeMaLen: d,
                  trendFilterMaPeriod: e,
                  percValue: percValValue,
                  drawdown: drawdownValue
                }
                results.push(o);
                perc = o.percValue.replace(' %', '');
                perc = parseFloat(perc);
                if (perc > betterPerc) {
                  betterPerc = perc;
                  betterByPerc = o;
                  console.log('BETTER BY PERCENTAGE', o);
                }
                drawdownNum = o.drawdown.replace(' %', '');
                drawdownNum = parseFloat(drawdownNum);
                if (drawdownNum < betterDrawdown) {
                  betterDrawdown = drawdownNum;
                  betterByDrawdown = o;
                  console.log('BETTER BY DRAWDOWN', o);
                }
              }
            }
          }
        }
      }
      // Save to file:
      console.log("DONE!");
    }
  }
  