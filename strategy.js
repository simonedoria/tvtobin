
const puppeteer = require('puppeteer');
require('dotenv').config();
var ccxt = require('ccxt');
const moment = require('moment-timezone');
moment.locale('it');

module.exports = {
    newCurrent: {},
    getNewCurrent: () => newCurrent != null ? newCurrent : {},
    async trendFollowingStrategy(ctx, pair, pairCcxt) {
        // apro puppeteer per prendere il close aggiornato
        console.log("Starting Browser...");
        ctx.telegram.sendMessage(102073553, 'Starting Browser...');
        const browser = await puppeteer.launch({
            headless: true,
            args: [
                '--start-maximized',
                '--no-sandbox'
            ]
        });
        console.log("Opening Page...");
        ctx.telegram.sendMessage(102073553, 'Opening Page...');
        const page = await browser.newPage();
        await page.setViewport({ width: 1920, height: 900 });
        console.log("Go to Login...");
        ctx.telegram.sendMessage(102073553, 'Go to Login...');
        await page.goto('https://it.tradingview.com', { waitUntil: 'networkidle2' });
        // LOGIN
        let btnMenuLogin = 'body > div.tv-main > div.tv-header.tv-header__top.js-site-header-container.tv-header--sticky.tv-header--promo.tv-header--animated > div.tv-header__inner > div.tv-header__area.tv-header__area--user > button.tv-header__user-menu-button.tv-header__user-menu-button--anonymous.js-header-user-menu-button';
        await page.waitForSelector(btnMenuLogin);
        await page.click(btnMenuLogin);
        let btnLogin = '.item-2IihgTnv';
        await page.waitForSelector(btnLogin);
        await page.click(btnLogin);
        let btnEmail = '.tv-signin-dialog__toggle-email';
        await page.waitForSelector(btnEmail);
        await page.click(btnEmail);
        let emailUserSelector = 'input[name=username]';
        let passUserSelector = 'input[name=password]';
        await page.waitForSelector(emailUserSelector);
        await page.waitForSelector(passUserSelector);
        await page.$eval(emailUserSelector, (el, value) => el.value = value, 'simonedoriasd@gmail.com');
        await page.$eval(passUserSelector, (el, value) => el.value = value, 'Anna.1992');
        let btnAccedi = '[id^=email-signin__submit-button__]';
        await page.waitForSelector(btnAccedi);
        console.log('Logging In....');
        ctx.telegram.sendMessage(102073553, 'Logging In...');
        await page.click(btnAccedi);
        await page.waitForTimeout(2000);
        console.log("Go to Chart...");
        ctx.telegram.sendMessage(102073553, 'Go to Chart...');
        //await page.goto(`https://it.tradingview.com/chart/WuM2FB8U/?symbol=BINANCE%3ABTCUSDTPERP`);
        //await page.waitForTimeout(1000);
        let selectorSearch = 'body > div.tv-main > div.tv-header.tv-header__top.js-site-header-container.tv-header--sticky > div.tv-header__inner > div.tv-header__middle-content > div.tv-header__area.tv-header__area--search > div > button.tv-header-search-container.tv-header-search-container__button.tv-header-search-container__button--full.js-header-search-button';
        await page.waitForSelector(selectorSearch);
        await page.click(selectorSearch);
        let inputSearchSelector = '#overlap-manager-root > div > div > div.wrap-2qEpRlNG > div > div > div.modal-2FY0kZ5K.dialog-2qEpRlNG.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp.fullscreen-UM6w7sFp > div > div.headerSearchInputBlock-3Sz6LtHP > span > form > input';
        await page.waitForSelector(inputSearchSelector);
        await page.click(inputSearchSelector);
        for (let i = 0; i < pair.length; i++) {
            //await page.$eval(inputSearchSelector, (el, value) => el.value += value, pair[i]);
            await page.keyboard.press(pair[i]);
            await page.waitForTimeout(200);
        }
        await page.waitForTimeout(2000);
        let firstElementSelector = '#overlap-manager-root > div > div > div.wrap-2qEpRlNG > div > div > div.modal-2FY0kZ5K.dialog-2qEpRlNG.dialog-UM6w7sFp.rounded-UM6w7sFp.shadowed-UM6w7sFp.fullscreen-UM6w7sFp > div > div.body-QONDOp3q > div > div.wrap-2WiKHt6t > div > div > div.itemRow-ZzQNZGNo > div.symbolDescription-ZzQNZGNo.cell-ZzQNZGNo.apply-overflow-tooltip.apply-overflow-tooltip--allow-text';
        await page.waitForSelector(firstElementSelector);
        await page.click(firstElementSelector);
        await page.waitForTimeout(3000);
        //hour settings
        let timeframeSelector = '#header-toolbar-intervals > div > div';
        await page.waitForSelector(timeframeSelector);
        await page.click(timeframeSelector);
        let h1Selector = '#overlap-manager-root > div > span > div.menuWrap-g78rwseC > div > div > div > div:nth-child(16)';
        let m15Selector = '#overlap-manager-root > div > span > div.menuWrap-g78rwseC > div > div > div > div:nth-child(11)';
        let m30Selector = '#overlap-manager-root > div > span > div.menuWrap-g78rwseC > div > div > div > div:nth-child(8)';
        await page.waitForSelector(m30Selector);
        await page.click(m30Selector);
        // pattern
        let patternSelector = '#header-toolbar-chart-styles > div';
        await page.waitForSelector(patternSelector);
        await page.click(patternSelector);
        // tab strategy open
        try {
            let ingranaggio = '#bottom-area > div.bottom-widgetbar-content.backtesting > div.backtesting-head-wrapper > div:nth-child(2)';
            page.waitForSelector(ingranaggio, { timeout: 1000 });
        } catch (exce) {
            let tabStrategyOpen = '#footer-chart-panel > div.tabbar-37voAVwR > div:nth-child(1) > div:nth-child(4)';
            page.waitForSelector(tabStrategyOpen, { timeout: 1000 });
            await page.click(tabStrategyOpen);
        }
        // opening dialog
        let listaOperazioniSelector = '#bottom-area > div.bottom-widgetbar-content.backtesting > div.backtesting-head-wrapper > div.backtesting-select-wrapper > ul > li:nth-child(3)';
        try {
            await page.waitForSelector(listaOperazioniSelector, { timeout: 1000 });
            await page.click(listaOperazioniSelector);
        } catch (eccListaOp) {
            let tabStrategyOpen = '#footer-chart-panel > div.tabbar-37voAVwR > div:nth-child(1) > div:nth-child(4)';
            page.waitForSelector(tabStrategyOpen, { timeout: 1000 });
            await page.click(tabStrategyOpen);
            await page.waitForSelector(listaOperazioniSelector, { timeout: 1000 });
            await page.click(listaOperazioniSelector);
        }
        // read table
        /*let report = await page.evaluate(() => {
            return document.querySelector('div > div > div > table > tbody:nth-child(12) > tr.row-e > td.trade-e-comment.comment');
        });
        console.log(report);*/
        ctx.telegram.sendMessage(102073553, 'Starting...');
        console.log("Starting...");
        let percentage = 6;
        let leverage = 10;
        let count = 1;
        let apiKey = process.env.API_KEY;
        let apisecret = process.env.API_SECRET;
        const client = new ccxt.binance({
            apiKey: apiKey,
            secret: apisecret,
            enableRateLimit: true,
            options: {
                defaultType: 'future'
            }
        });
        await client.load_markets();
        let mkt = client.markets[pairCcxt];
        await client.fapiPrivate_post_leverage({
            symbol: mkt['id'],
            leverage: leverage,
        })
        ctx.telegram.sendMessage(102073553, `Initial Scrolling...`);
        let data = await page.$$eval('.reports-content__table-pointer tbody', tds => tds.map((td) => {
            return td.innerText;
        }));
        let dataArr = data[data.length - 1].split(/\s+/);
        this.newCurrent = {
            entryType: dataArr[3],
            entryDate: `${dataArr[4]} ${dataArr[5]}`,
            entryPrice: dataArr[6],
            tradeValue: dataArr.length >= 10 ? dataArr[8] : null,
            exitType: dataArr.length >= 10 ? dataArr[22] : null,
            exitDate: dataArr.length >= 10 ? `${dataArr[23]} ${dataArr[24]}` : null,
            exitPrice: dataArr.length >= 10 ? dataArr[25] : null,
            eventDate: moment().format(),
            priceWhenOrder: null
        }
        console.log("newCurrent", this.newCurrent);
        ctx.telegram.sendMessage(102073553, `Let's magic begin...`);
        console.log("Let's magic begin....")
        let equichesuccedelamagia = async () => {
            await page.$eval('.table-wrap', e => {
                e.scrollIntoView({ block: 'end', inline: 'end' });
            });
            data = await page.$$eval('.reports-content__table-pointer tbody', tds => tds.map((td) => {
                return td.innerText;
            }));
            dataArr = data[data.length - 1].split(/\s+/);
            //console.log("dataArr", dataArr);
            let element = {
                entryType: dataArr[3],
                entryDate: `${dataArr[4]} ${dataArr[5]}`,
                entryPrice: dataArr[6],
                tradeValue: dataArr.length >= 10 ? dataArr[8] : null,
                exitType: dataArr.length >= 10 ? dataArr[22] : null,
                exitDate: dataArr.length >= 10 ? `${dataArr[23]} ${dataArr[24]}` : null,
                exitPrice: dataArr.length >= 10 ? dataArr[25] : null,
                eventDate: moment().format(),
                priceWhenOrder: null
            }
            if ((this.newCurrent.entryDate !== element.entryDate || this.newCurrent.exitDate !== element.exitDate)
                && (element.entryType == 'SLPLong' || element.entryType == 'SLPShort')) {
                console.log("element", element);
                ctx.telegram.sendMessage(102073553, "telegram msg " + JSON.stringify(newCurrent));
                console.log("newCurrent", this.newCurrent);
                // apertura ordine
                let order = null;
                let value = 0;
                // se nel record precedente, NON c'era una chiusura, allora ora c'è, chiudo tutto
                if (this.newCurrent.exitType == null && element.exitType != null) {
                    let positions = await client.fetchBalance();
                    positions = positions.info.positions;
                    let symbolPosition = positions.filter(p => p.symbol = mkt['id']);
                    symbolPosition = symbolPosition.filter(p => parseFloat(p['positionAmt']) != 0);
                    if (this.newCurrent.entryType === 'SLPLong') {
                        ctx.telegram.sendMessage(102073553, `Segnale di chiusura long a ${this.newCurrent.exitPrice}`);
                    }
                    if (this.newCurrent.entryType === 'SLPShort') {
                        ctx.telegram.sendMessage(102073553, `Segnale di chiusura short a ${this.newCurrent.exitPrice}`);
                    }
                    if (symbolPosition.length > 0) {
                        value = symbolPosition[0].positionAmt;
                        order = await client.createOrder(mkt['symbol'], 'MARKET', value < 0 ? 'BUY' : 'SELL', amount = Math.abs(value));
                        ctx.telegram.sendMessage(102073553, JSON.stringify(order));
                        this.newCurrent = element;
                    }
                } else {
                    // altrimenti acquisto long o short
                    let market = await client.fetchTicker(pairCcxt);
                    let priceWhenOrder = market.info.lastPrice;
                    let balances = await client.fetchBalance();
                    let avBalance = balances.info.availableBalance;
                    value = (avBalance * (percentage / 100) * leverage) / priceWhenOrder;
                    // chiudo posizione aperta in caso di inversione repentina
                    let positions = await client.fetchBalance();
                    positions = positions.info.positions;
                    let symbolPosition = positions.filter(p => p.symbol = mkt['id']);
                    symbolPosition = symbolPosition.filter(p => parseFloat(p['positionAmt']) != 0);
                    if (this.newCurrent.entryType === 'SLPLong') {
                        ctx.telegram.sendMessage(102073553, `Segnale di chiusura long a ${this.newCurrent.exitPrice}`);
                    }
                    if (this.newCurrent.entryType === 'SLPShort') {
                        ctx.telegram.sendMessage(102073553, `Segnale di chiusura short a ${this.newCurrent.exitPrice}`);
                    }
                    if (symbolPosition.length > 0) {
                        value = symbolPosition[0].positionAmt;
                        order = await client.createOrder(mkt['symbol'], 'MARKET', value < 0 ? 'BUY' : 'SELL', amount = Math.abs(value));
                        ctx.telegram.sendMessage(102073553, JSON.stringify(order));
                        this.newCurrent = element;
                    }
                    if (element.entryType === 'SLPLong') {
                        order = await client.createOrder(mkt['symbol'], 'MARKET', 'BUY', value);
                        ctx.telegram.sendMessage(102073553, JSON.stringify(order));
                        element.priceWhenOrder = priceWhenOrder;
                        ctx.telegram.sendMessage(102073553, `Segnale di apertura long a ${newCurrent.entryPrice} (TV) e ${newCurrent.priceWhenOrder} (Binance)`);
                        this.newCurrent = element;
                    }
                    if (element.entryType === 'SLPShort') {
                        order = await client.createOrder(mkt['symbol'], 'MARKET', 'SELL', value);
                        ctx.telegram.sendMessage(102073553, JSON.stringify(order));
                        element.priceWhenOrder = priceWhenOrder;
                        ctx.telegram.sendMessage(102073553, `Segnale di apertura short a ${newCurrent.entryPrice} (TV) e ${newCurrent.priceWhenOrder} (Binance)`);
                        this.newCurrent = element;
                    }
                }
            }
            page.waitForTimeout(1000);
            equichesuccedelamagia();
        };
        equichesuccedelamagia();
    }
}