const express = require('express')
const port = 9999;
const routes = require('./controller.js')
const app = express();

app.use('/v1/', routes);

app.listen(port, () => {
  console.log(`TvCrypto listening on port: ${port}`)
});
